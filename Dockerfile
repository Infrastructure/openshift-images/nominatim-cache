FROM registry.access.redhat.com/ubi9/python-311:1
ENV PYTHONPATH=/opt/app-root/src

RUN pip install fastapi pydantic-settings uvicorn redis hiredis httpx
ADD app/main.py /opt/app-root/src

ENTRYPOINT ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000"]
