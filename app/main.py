import hashlib

import httpx
from fastapi import BackgroundTasks, FastAPI, Request, Response
from pydantic_settings import BaseSettings
from redis import asyncio as aioredis


class Settings(BaseSettings):
    api_url: str = "https://nominatim.openstreetmap.org"
    redis_host: str = "redis"
    redis_port: int = 6379
    redis_expiry: int = 3600


settings = Settings()

redis = aioredis.Redis(
    host=settings.redis_host, port=settings.redis_port, decode_responses=True
)

app = FastAPI()


async def redis_setex(key, value, timeout=settings.redis_expiry):
    async with redis.client() as redis_conn:
        await redis_conn.setex(key, timeout, value)


async def redis_expire(key, timeout=settings.redis_expiry):
    async with redis.client() as redis_conn:
        await redis_conn.expire(key, timeout)


@app.get("/status")
async def healthcheck():
    return {"status": "OK"}


@app.get("/search")
async def search(request: Request, background_tasks: BackgroundTasks):
    lang = request.query_params.get("accept-language", "none")
    limit = request.query_params.get("limit", "none")
    country = request.query_params.get("country", "none")
    state = request.query_params.get("state", "none")
    city = request.query_params.get("city", "none")
    street = request.query_params.get("street", "none")

    location = hashlib.md5(f"{country}:{state}:{city}:{street}".encode()).hexdigest()

    if query := request.query_params.get("q"):
        query = hashlib.md5(query.encode()).hexdigest()
    else:
        query = "none"

    key = f"search:{lang}:{limit}:{location}:{query}"

    async with redis.client() as redis_conn:
        if resp := await redis_conn.get(key):
            background_tasks.add_task(redis_expire, key)
            return Response(content=resp, media_type="application/json")

    async with httpx.AsyncClient() as client:
        r = await client.get(f"{settings.api_url}/search", params=request.query_params)
        background_tasks.add_task(redis_setex, key, r.text)

        return Response(
            content=r.text, media_type="application/json", status_code=r.status_code
        )


@app.get("/reverse")
async def reverse(request: Request, background_tasks: BackgroundTasks):
    lat = "{:.5f}".format(float(request.query_params["lat"]))
    lon = "{:.5f}".format(float(request.query_params["lon"]))
    lang = request.query_params.get("accept-language", "none")

    key = f"rev:{lang}:{lat}x{lon}"
    async with redis.client() as redis_conn:
        if resp := await redis_conn.get(key):
            background_tasks.add_task(redis_expire, key)
            return Response(content=resp, media_type="application/json")

    query_params = {
        "lat": lat,
        "lon": lon,
        "addressdetails": 1,
        "accept-language": lang,
        "format": "json",
    }

    async with httpx.AsyncClient() as client:
        r = await client.get(f"{settings.api_url}/reverse", params=query_params)
        background_tasks.add_task(redis_setex, key, r.text)

        return Response(
            content=r.text, media_type="application/json", status_code=r.status_code
        )
